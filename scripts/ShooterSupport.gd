extends Area2D
class_name ShooterSupport

export (float) var attack_range = 200
export (float) var attack_speed = 1
export (PackedScene) var projectile
var target
var parent
var bonus = 4
var bonus_count = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if attack_speed == 0:
		print_debug("attack speed 0!")
		return
	parent = get_node("..")
	var collision_shape = CollisionShape2D.new()
	var shape = CircleShape2D.new()
	shape.radius = attack_range
	collision_shape.set_shape(shape)
	add_child(collision_shape)
	var timer = Timer.new()
	add_child(timer)
	timer.start(1 / attack_speed)
	timer.connect("timeout", self, "shoot")

func shoot():	
	if get_node("/root/Main").state != "playing": return
	if target && !overlaps_area(target):
		target = null
	if !target:
		var overlapping = get_overlapping_areas()
		for obj in overlapping:
			target = obj
			break
	if target and target.name == 'Tower':
		print(bonus_count)
		if bonus_count <= 2:
			target.get_node("Shooter").bonus = target.get_node("Shooter").bonus + target.get_node("Shooter").bonus * 0.20
			bonus_count = bonus_count + 1
			var bullet = projectile.instance()
			parent.get_node("..").add_child(bullet)
			bullet.collision_layer = 0
			bullet.collision_mask = collision_mask
			bullet.position = parent.position
			bullet.fire((target.position - parent.position).normalized())
		else:
			get_parent().queue_free()
